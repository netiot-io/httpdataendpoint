FROM anapsix/alpine-java:8
ADD target/httpdataendpoint.jar httpdataendpoint.jar
ENV JAVA_OPTS=""
ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /httpdataendpoint.jar
