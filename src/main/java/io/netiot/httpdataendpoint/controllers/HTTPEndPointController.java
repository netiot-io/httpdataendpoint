package io.netiot.httpdataendpoint.controllers;

import io.netiot.httpdataendpoint.models.DeviceDataModel;
import io.netiot.httpdataendpoint.models.DevicesModel;
import io.netiot.httpdataendpoint.services.DevicesService;
import io.netiot.httpdataendpoint.services.KafkaMessageSenderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Calendar;

@Slf4j
@RestController
@RequiredArgsConstructor
public class HTTPEndPointController {
    private final KafkaMessageSenderService kafkaSender;

    private final DevicesService devicesService;

    @PostMapping("/deviceData/{uuid}")
    public ResponseEntity saveData(@PathVariable(name = "uuid") String uuid,
                                                   @RequestBody @Valid DevicesModel data) {

        //log.info("Received data: " + data);

        Calendar calendar = Calendar.getInstance();
        long now = calendar.getTimeInMillis();

        Long id = devicesService.getDeviceId(uuid);

        for (DeviceDataModel deviceData : data.getDevices()) {
            deviceData.setDeviceId(id);
            deviceData.setTimestamp_r(now);
        }

        kafkaSender.sendEvent(data);

        return ResponseEntity.ok().build();
    }
}