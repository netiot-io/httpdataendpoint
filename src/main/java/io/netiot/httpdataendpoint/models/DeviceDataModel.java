package io.netiot.httpdataendpoint.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class DeviceDataModel {
    private Long deviceId;
    private String data;
    private Long timestamp;
    private Long timestamp_r;
}
