# HTTPdataendpoint

Ingests data from http requests into the platform

## Known paths
``POST httpdataendpoint/V1/deviceData/{uuid}``

``Body``
```
{
    "devices":[
        {
            "data": "0100F3023E0400B50500070DB615482F",
            "timestamp": 1561701861000
        }
    ]
}
```

## Feign
Communicates with the `devices` microservice to get the device `id` based on its `uuid`.

`GET devices/V1/devices/internal/{uuid}/id`

Return: `long`

## Kafka
Uses kafka to send the received data to the kafka broker on the specified topic.

## CI/CD
Compile, test, package, build docker image and push to gitlab docker registry.

## Tests
N/A

